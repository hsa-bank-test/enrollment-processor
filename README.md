#### To Run App:
Change to Processor directory `cd Proccessor`  
Run against available csv files:

 - Simple valid input :  `dotnet run simple.csv`
 - Invalid input file: `dotnet run invalid_input.csv`
 
#### Example CI/CD output:  
- [build results](https://gitlab.com/hsa-bank-test/enrollment-processor/-/jobs/543401376)
- [test results](https://gitlab.com/hsa-bank-test/enrollment-processor/-/jobs/543401379)
- [run results](https://gitlab.com/hsa-bank-test/enrollment-processor/-/jobs/543401381)