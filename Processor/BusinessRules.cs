namespace Processor
{
    using System;

    public class BusinessRules
    {
        private readonly DateTime today;

        public BusinessRules()
        {
            this.today = DateTime.Now.Date;
        }

        public BusinessRules(DateTime today)
        {
            this.today = today;
        }

        public IDisplayRecord Run(DetailRecord record)
        {
            if (record.DateOfBirth.AddYears(18).CompareTo(today) > 0)
                return new RejectedRecord(record);
            if (record.EffectiveDate.AddDays(-30).CompareTo(today) > 0)
                return new RejectedRecord(record);
            return new AcceptedRecord(record);
        }
    }
}