namespace Processor
{
    public interface IDisplayRecord
    {
        string Display { get; }
    }

    public class RejectedRecord : IDisplayRecord
    {
        public string Display => "Rejected, " + display;

        public RejectedRecord(DetailRecord record)
        {
            display = record.Display;
        }

        private string display;
    }

    public class AcceptedRecord : IDisplayRecord
    {
        public string Display => "Accepted, " + display;

        public AcceptedRecord(DetailRecord record)
        {
            display = record.Display;
        }

        private string display;
    }
}