﻿using System;
using System.IO;

namespace Processor
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 1 || !File.Exists(args[0]))
            {
                Console.WriteLine("Please provide a file to process.");
                return;
            }

            var file = File.ReadAllLines(args[0]);
            var processor = new FileProcessor(file);

            foreach (var result in processor.Process())
            {
                Console.WriteLine(result);
            }
        }
    }
}
