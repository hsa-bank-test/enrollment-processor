namespace Processor
{
    using System;
    using System.Globalization;

    public enum Plan
    {
        HSA,
        HRA,
        FSA
    }

    public class DetailRecord
    {
        public string Display =>
            $"{FirstName}, {LastName}, {DateOfBirth.ToShortDateString()}, {PlanType}, {EffectiveDate.ToShortDateString()}";

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Plan PlanType { get; set; }
        public DateTime EffectiveDate { get; set; }

        public static DetailRecord DecodeLine(string line)
        {
            var fields = line.Split(',');
            if (fields.Length != 5)
                throw new InvalidRecord();
            return new DetailRecord
            {
                FirstName = fields[0],
                LastName = fields[1],
                DateOfBirth = ValidateDate(fields[2]),
                PlanType = ValidatePlan(fields[3]),
                EffectiveDate = ValidateDate(fields[4])
            };
        }

        public static DateTime ValidateDate(string date)
        {
            if (DateTime.TryParseExact(date, "MMddyyyy", null, DateTimeStyles.AllowLeadingWhite, out DateTime result))
            {
                return result;
            }
            else
            {
                throw new InvalidRecord();
            }
        }

        public static Plan ValidatePlan(string plan) =>
            plan.ToUpper() switch
            {
                "HSA" => Plan.HSA,
                "HRA" => Plan.HRA,
                "FSA" => Plan.FSA,
                _ => throw new InvalidRecord()
            };

    }
}