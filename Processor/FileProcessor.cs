using System.Collections.Generic;
using System.Linq;

namespace Processor
{
    public class FileProcessor
    {
        public FileProcessor(string[] file)
        {
            this.file = file;
            this.rules = new BusinessRules();
        }

        private string[] file;
        private BusinessRules rules;

        public List<string> Process()
        {
            try
            {
                return file
                    .Select(DetailRecord.DecodeLine)
                    .Select(rules.Run)
                    .Select(record => record.Display)
                    .ToList();
            }
            catch (InvalidRecord)
            {
                return new[] { "A record in the file failed validation. Processing has stopped." }.ToList();
            }
        }
    }
}