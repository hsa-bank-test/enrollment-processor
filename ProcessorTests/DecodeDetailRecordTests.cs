namespace ProcessorTests
{
    using Xunit;
    using Processor;
    using System;

    public class DecodeDetailRecordTests
    {
        [Fact]
        public void CreateDetailRecordSuccessfully()
        {
            var line = "First,Last,04111978,HRA,01012020";
            var result = DetailRecord.DecodeLine(line);

            Assert.Equal("First", result.FirstName);
            Assert.Equal("Last", result.LastName);
            Assert.Equal("1978-04-11", result.DateOfBirth.ToString("yyyy-MM-dd"));
            Assert.Equal(Plan.HRA, result.PlanType);
            Assert.Equal("2020-01-01", result.EffectiveDate.ToString("yyyy-MM-dd"));
        }

        [Fact]
        public void AllFieldsAreRequired()
        {
            var line = "Missing Many fields";
            Assert.Throws<InvalidRecord>(() => DetailRecord.DecodeLine(line));
        }

        [Fact]
        public void DatesMustBeFormattedCorrectly()
        {
            var date = "01012020";
            var expected = new DateTime(2020, 1, 1);
            Assert.Equal(expected, DetailRecord.ValidateDate(date));
        }

        [Theory]
        [InlineData("x1x2020")]
        [InlineData("bad")]
        [InlineData("")]
        public void PoorlyFormattedDatesAreInvalid(string date)
        {
            Assert.Throws<InvalidRecord>(() => DetailRecord.ValidateDate(date));
        }

        [Theory]
        [InlineData("HSA", Plan.HSA)]
        [InlineData("HRA", Plan.HRA)]
        [InlineData("FSA", Plan.FSA)]
        [InlineData("hsa", Plan.HSA)]
        [InlineData("hra", Plan.HRA)]
        [InlineData("fsa", Plan.FSA)]
        public void PlanMustBeFromAllowedSet(string candidatePlan, Plan expected)
        {
            var result = DetailRecord.ValidatePlan(candidatePlan);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("xyz")]
        [InlineData("")]
        [InlineData("HSA2")]
        public void UnallowedPlansAreInvalid(string candidatePlan)
        {
            Assert.Throws<InvalidRecord>(() => DetailRecord.ValidatePlan(candidatePlan));
        }
    }
}