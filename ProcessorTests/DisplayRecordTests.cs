namespace ProcessorTests
{
    using Xunit;
    using Processor;
    using System;

    public class DisplayRecordTests
    {
        [Fact]
        public void DisplayDetailRecord()
        {
            var line = "First,Last,04111978,HSA,01012020";
            var result = DetailRecord.DecodeLine(line);
            var expected = $"First, Last, {ShortDate(1978, 4, 11)}, HSA, {ShortDate(2020, 1, 1)}";

            Assert.Equal(expected, result.Display);
        }

        [Fact]
        public void DisplayAcceptedRecord()
        {
            var record = DetailRecord.DecodeLine("First,Last,04111978,HSA,01012020");
            var result = new AcceptedRecord(record);
            var expected = $"Accepted, First, Last, {ShortDate(1978, 4, 11)}, HSA, {ShortDate(2020, 1, 1)}";

            Assert.Equal(expected, result.Display);
        }

        [Fact]
        public void DisplayRejectedRecord()
        {
            var record = DetailRecord.DecodeLine("First,Last,04111978,HSA,12122020");
            var result = new RejectedRecord(record);
            var expected = $"Rejected, First, Last, {ShortDate(1978, 4, 11)}, HSA, {ShortDate(2020, 12, 12)}";

            Assert.Equal(expected, result.Display);
        }

        private static string ShortDate(int year, int month, int day) =>
            new DateTime(year, month, day).ToShortDateString();
    }
}