namespace ProcessorTests
{
    using Xunit;
    using Processor;

    public class FileProcessorTests
    {
        [Fact]
        public void ValidInputsPrintALineForEach()
        {
            var lines = new[]
            {
                "First,Last,04111978,HSA,01012020",
                "Ben,Davis,04111978,HSA,01022020",
            };

            var result = new FileProcessor(lines).Process();
            Assert.Equal(lines.Length, result.Count);
        }

        [Fact]
        public void FailedValidationReturnsASingleLineError()
        {
            var lines = new[]
            {
                "Missing Fields",
                "Ben,Davis,04111978,HSA,01022020",
            };

            var result = new FileProcessor(lines).Process();
            Assert.Single(result);
        }
    }
}