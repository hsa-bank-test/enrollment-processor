namespace ProcessorTests
{
    using System;
    using Processor;
    using Xunit;

    public class BusinessRulesTests
    {
        DateTime Today = new DateTime(2020, 5, 1);

        [Fact]
        public void RecordsUnder18AreRejected()
        {
            var record = AgedRecord(17);
            var result = new BusinessRules(Today).Run(record);

            Assert.StartsWith("Rejected, ", result.Display);
            Assert.IsType<RejectedRecord>(result);
        }

        [Theory]
        [InlineData(18)]
        [InlineData(45)]
        public void Records18AndOlderAreAccepted(int age)
        {
            var record = AgedRecord(age);
            var result = new BusinessRules(Today).Run(record);

            Assert.StartsWith("Accepted, ", result.Display);
            Assert.IsType<AcceptedRecord>(result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(30)]
        public void RecordsWithin30DaysAreAccepted(int daysOut)
        {
            var record = FutureRecord(daysOut);
            var result = new BusinessRules(Today).Run(record);

            Assert.IsType<AcceptedRecord>(result);
        }

        [Theory]
        [InlineData(31)]
        [InlineData(60)]
        public void RecordsMoreThan30DaysInTheFutureAreRejected(int daysOut)
        {
            var record = FutureRecord(daysOut);
            var result = new BusinessRules(Today).Run(record);

            Assert.IsType<RejectedRecord>(result);
        }


        private DetailRecord AgedRecord(int age) =>
            new DetailRecord
            {
                FirstName = "First",
                LastName = "Last",
                DateOfBirth = Today.AddYears(-age),
                EffectiveDate = Today,
                PlanType = Plan.HSA
            };

        private DetailRecord FutureRecord(int days) =>
            new DetailRecord
            {
                FirstName = "First",
                LastName = "Last",
                DateOfBirth = Today.AddYears(-30),
                EffectiveDate = Today.AddDays(days),
                PlanType = Plan.HSA
            };
    }
}